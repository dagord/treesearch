#!/usr/bin/python2.7
import os
import xml.etree.ElementTree as ET
import re
import datetime

logfile = open('./regex.log', 'a+')

def tree(albero, spaces = ""):
	global logfile
	for child in albero:
		substring = re.findall("'\w*'", str(child.attrib))
		if substring:
			attrib = substring[1].replace("'", '')
		else:
			attrib = ''

		try:
			subtext = str(child.text)
			match = True
		except:
			subtext = str(re.findall("[\x00-\x7F]*", unicode(child.text))[0])
			match = False

		line = "%s %s: %s (%s)" % (spaces, attrib, subtext, match)
		print line
		logfile.write("%s\n" % line)

		if child.attrib is not None:
			tree(child, spaces + "\t")


files = os.listdir('.')
logfile.write("***************\n")
logfile.write("%s\n" % datetime.datetime.now())
for file in files:
	if os.path.isfile(file) and '.xml' in file:
		logfile.write("***************\n")
		logfile.write("%s\n" % file)
		e = ET.parse(file).getroot()

		tree(e)

logfile.close()
